#!/usr/bin/env python
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('ref_dict')
args = parser.parse_args()

sequence_tuple_list = []
longest_sequence = 0

with open(args.ref_dict, 'rt') as ref_dict_file:
    for line in ref_dict_file:
        if line.startswith('@SQ'):
            line_split = line.split('\t')
            sequence_tuple_list.append((line_split[1].split('SN:')[1], int(line_split[2].split('LN:')[1])))

longest_sequence = sorted(sequence_tuple_list, key=lambda x: x[1], reverse=True)[0][1]

print(longest_sequence)

hg38_protection_tag = ':1+'

tsv_string = sequence_tuple_list[0][0] + hg38_protection_tag
temp_size = sequence_tuple_list[0][1]
for sequence_tuple in sequence_tuple_list[1:]:
    if temp_size + sequence_tuple[1] <= longest_sequence:
        temp_size += sequence_tuple[1]
        tsv_string += "\t" + sequence_tuple[0] + hg38_protection_tag
    else:
        tsv_string += '\n' + sequence_tuple[0] + hg38_protection_tag
        temp_size = sequence_tuple[1]

with open('sequence_grouping.txt', 'wt') as tsv_file:
    tsv_file.write(tsv_string)

tsv_string += '\n' + 'unmapped'

with open('sequence_grouping_with_unmapped.txt', 'wt') as tsv_file_with_unmapped:
    tsv_file_with_unmapped.write(tsv_string)
