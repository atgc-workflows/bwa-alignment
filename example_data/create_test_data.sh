#!/bin/sh
set -o errexit

rm -f genome.fa genome.fa.fai genome.dict genome.ndx
rm -f *.fq *.fq.gz

python3 random_genome.py 10000 >genome.fa
samtools faidx genome.fa
samtools dict genome.fa >genome.dict

novoindex -k 14 genome.ndx genome.fa


wgsim -N 100 -e 0 -1 75 -2 75 genome.fa tumour_1_1.fq tumour_1_2.fq
wgsim -N 100 -e 0 -1 75 -2 75 genome.fa tumour_2_1.fq tumour_2_2.fq
wgsim -N 100 -e 0 -1 75 -2 75 genome.fa tumour_3_1.fq tumour_3_2.fq
wgsim -N 100 -e 0 -1 75 -2 75 genome.fa tumour_4_1.fq tumour_4_2.fq
wgsim -N 100 -e 0 -1 75 -2 75 genome.fa normal_1_1.fq normal_1_2.fq
wgsim -N 100 -e 0 -1 75 -2 75 genome.fa normal_2_1.fq normal_2_2.fq
wgsim -N 100 -e 0 -1 75 -2 75 genome.fa normal_3_1.fq normal_3_2.fq
wgsim -N 100 -e 0 -1 75 -2 75 genome.fa normal_4_1.fq normal_4_2.fq
gzip *.fq
