#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import textwrap
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('n', nargs='?', type=int, default=1000)
args = parser.parse_args()

header = f'>random sequence 1 consisting of {args.n} bases.'
print(header)
print(textwrap.fill(''.join(random.choice(['A', 'T', 'G', 'C']) for i in range(args.n)), 60))
