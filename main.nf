/**
 * GATKs official workflow scatters the recalibration step (over what though?)
 * Creates contig level splits (contigs may not be split). Must include
 * unmapped reads (or they will be lost).
 *
 * Also QC
 * 
 * CollectUnsortedReadgroupBamQualityMetrics
 * CrossCheckFingerPrints (only if haplotype_database_file is defined)
 * CheckContamination
 */
params.userinput = false
params.resources = false
params.outdir = false

if (!params.userinput) {
	exit 1, "parameter --userinput is required"
}

if (!params.resources) {
	exit 1, "parameter --resources is required"
}

if (!params.outdir) {
	exit 1, "parameter --outdir is required"
}

def reader = new groovy.json.JsonSlurper()
def user_input = reader.parse(new java.io.File(params.userinput))

def resources = reader.parse(new java.io.File(params.resources))

refFasta = file(resources["ref_fasta"])
compressionLevel = 2

dbsnpVcf = file(resources["dbsnp_vcf"])
dbsnpVcfIndex = file(resources["dbsnp_vcf_index"])

knownIndels = Channel.value(resources["known_indels"].collect { file(it) })
knownIndelsIndexes = Channel.value(resources["known_indels_indexes"].collect { file(it) })

sampleId = user_input["sample_id"]
xs = Channel.from(user_input["reads"].collect {
	[it["readgroup"]["id"], it["readgroup"]["sm"], it["readgroup"]["lb"], 
	 it["readgroup"]["pu"], it["readgroup"]["pl"], it["readgroup"]["cn"],
	 it["readgroup"]["pm"], it["readgroup"]["dt"], 
	 file(it["read1"]), file(it["read2"])]
})

bwa_container = "overcookedfrog/bwa:0.7.17"


process fastaqToSam {

    input:
    set id, sm, lb, pu, pl, cn, pm, runDate, file(f1), file(f2) from xs

    output:
    set id, file("${id}.unmapped.bam") into readgroupBam

    publishDir params.outdir + '/ubams', mode: 'copy'
    container bwa_container

    """
    java -jar /picard.jar FastqToSam F1=$f1 F2=$f2 O=${id}.unmapped.bam \
      READ_GROUP_NAME=$id SM=$sm LB=$lb PU=$pu PL=$pl CN=$cn PM=$pm \
      RUN_DATE=$runDate
    """
}

process indexReference {
	input:
	file refFasta

	output:
	file "${refFasta}.fai" into refFastaFai

	container bwa_container

	"""
	samtools faidx $refFasta
	"""
}

process createSequenceDictionary {
	input:
	file refFasta

	output:
	file "${refFasta.baseName}.dict" into refFastaDict

	container bwa_container 

	"""
	samtools dict $refFasta >${refFasta.baseName}.dict
	"""
}

process getBwaVersion {
	output:
	file 'version.txt' into bwaVersion

	container bwa_container

	"""
	bwa 2>&1 | grep -e '^Version' | sed 's/Version: //' >version.txt
	"""
}

process createBwaIndex {
    input:
    file refFasta

    output:
    file "*.{amb,ann,bwt,pac,sa,dict}" into bwaIndex

    container bwa_container

    """
    bwa index -a bwtsw $refFasta
    samtools dict $refFasta >${refFasta.baseName}.dict
    """
}


process samToFastqAndBwaMem {

    input:
    set id, file(inputBam) from readgroupBam
    file(bwaIndex) from bwaIndex
    file refFasta
	val compressionLevel 
	val bwaVersion

	output:
	file "${id}.bam" into laneBam

	publishDir params.outdir + '/bam', mode: 'copy'
	container bwa_container

    """
	set -o pipefail
	set -o errexit

    java -Xms1000m -Xmx1000m -jar /picard.jar \
        SamToFastq \
        INPUT=$inputBam \
        FASTQ=/dev/stdout \
        INTERLEAVE=true \
        NON_PF=true | \
      bwa mem -K 100000000 -p -v 3 -t 16 -Y $refFasta /dev/stdin - 2> bwa.stderr.log | \
      java -Dsamjdk.compression_level=$compressionLevel -Xms1000m -Xmx1000m -jar /picard.jar \
        MergeBamAlignment \
        VALIDATION_STRINGENCY=SILENT \
        EXPECTED_ORIENTATIONS=FR \
        ATTRIBUTES_TO_RETAIN=X0 \
        ATTRIBUTES_TO_REMOVE=NM \
        ATTRIBUTES_TO_REMOVE=MD \
        ALIGNED_BAM=/dev/stdin \
        UNMAPPED_BAM=$inputBam \
        OUTPUT=${id}.bam \
        REFERENCE_SEQUENCE=$refFasta \
        PAIRED_RUN=true \
        SORT_ORDER="unsorted" \
        IS_BISULFITE_SEQUENCE=false \
        ALIGNED_READS_ONLY=false \
        CLIP_ADAPTERS=false \
        MAX_RECORDS_IN_RAM=2000000 \
        ADD_MATE_CIGAR=true \
        MAX_INSERTIONS_OR_DELETIONS=-1 \
        PRIMARY_ALIGNMENT_STRATEGY=MostDistant \
        PROGRAM_RECORD_ID="bwamem" \
        PROGRAM_GROUP_VERSION="\$(cat $bwaVersion)" \
        PROGRAM_GROUP_COMMAND_LINE="bwa" \
        PROGRAM_GROUP_NAME="bwamem" \
        UNMAPPED_READ_STRATEGY=COPY_TO_TAG \
        ALIGNER_PROPER_PAIR_FLAGS=true \
        UNMAP_CONTAMINANT_READS=true \
        ADD_PG_TAG_TO_READS=false
    """
}

process markDuplicates {

    input:
    file laneBam from laneBam.collect()
    val compressionLevel
    val outputBamBaseName from sampleId

    output:
    file "${outputBamBaseName}.bam" into sampleBam

    publishDir params.outdir + "/bam", mode: 'copy'
    container bwa_container

    script:
    def inputs = "INPUT=" + laneBam.join(" INPUT=")
    """
    java -Dsamjdk.compression_level=$compressionLevel -Xms4000m -jar /picard.jar \
      MarkDuplicates \
      ${inputs} \
      OUTPUT=${outputBamBaseName}.bam \
      METRICS_FILE=${outputBamBaseName}.metrics.txt \
      VALIDATION_STRINGENCY=SILENT \
      OPTICAL_DUPLICATE_PIXEL_DISTANCE=2500 \
      ASSUME_SORT_ORDER="queryname" \
      CLEAR_DT="false" \
      ADD_PG_TAG_TO_READS=false \
    """
}

process sortSam {
    input:
    file inputBam from sampleBam
    val outputBasename from sampleId

    output:
    file "${outputBasename}.sorted.bam" into sortedBam
    file "${outputBasename}.sorted.bai" into sortedBamIndex
    file "${outputBasename}.sorted.bam.md5" into sortedBamMd5

    publishDir params.outdir + "/bam", mode: 'copy'
    container bwa_container

    """
    java -Dsamjdk.compression_level=$compressionLevel -Xms4000m -jar /picard.jar \
      SortSam \
      INPUT=$inputBam \
      OUTPUT=${outputBasename}.sorted.bam \
      SORT_ORDER="coordinate" \
      CREATE_INDEX=true \
      CREATE_MD5_FILE=true \
      MAX_RECORDS_IN_RAM=300000
    """
}

process baseRecalibration {

    input:
    file inputBam from sortedBam
    file inputBamIndex from sortedBamIndex
    file refFasta
	file refFastaFai
	file refFastaDict
    file dbsnpVcf
    file dbsnpVcfIndex
    file knownIndels
    file knownIndelsIndexes
    val outputBasename from sampleId

    output:
    file "${outputBasename}.recal" into recalReport

    publishDir params.outdir + "/bam", mode: "copy"
    container "broadinstitute/gatk:4.1.0.0"

    script:
    def indels = "--known-sites " + knownIndels.join(" --known-sites ")

    """
    gatk --java-options "-XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10 -XX:+PrintFlagsFinal \
      -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -XX:+PrintGCDetails \
      -Xloggc:gc_log.log -Xms4000m" \
      BaseRecalibrator \
      -R ${refFasta} \
      -I ${inputBam} \
      --use-original-qualities \
      -O ${outputBasename}.recal \
      --known-sites ${dbsnpVcf} \
      ${indels}
    """
}

process applyRecalibration {

	input:
	file inputBam from sortedBam
	file inputBamIndex from sortedBamIndex
	file recalReport
	file refFasta
	file refFastaFai
	file refFastaDict
	val compressionLevel

	output:
	file "${inputBam.baseName}.recalibrated.bam" into recalBam
	file "${inputBam.baseName}.recalibrated.bai" into recalBamIndex
	file "${inputBam.baseName}.recalibrated.bam.md5" into recalBamMd5

	publishDir params.outdir + '/bam', mode: 'copy'
	container 'broadinstitute/gatk:4.1.0.0'

	"""
	gatk --java-options "-XX:+PrintFlagsFinal -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps \
	  -XX:+PrintGCDetails -Xloggc:gc_log.log \
	  -XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10 -Dsamjdk.compression_level=${compressionLevel} -Xms3000m" \
	  ApplyBQSR \
	  --create-output-bam-md5 \
	  --add-output-sam-program-record \
	  -R ${refFasta} \
	  -I ${inputBam} \
	  --use-original-qualities \
	  -O ${inputBam.baseName}.recalibrated.bam \
	  -bqsr ${recalReport} \
	  --static-quantized-quals 10 \
	  --static-quantized-quals 20 \
	  --static-quantized-quals 30 \
	"""
}
