# Example BAM Processing Workflow

This is a nextflow workflow that implements the Broad's BAM processing pipeline from:

https://github.com/gatk-workflows/gatk4-exome-analysis-pipeline

without the QC steps.

You can run the example data through the workflow with:

```sh
nextflow run main.nf --userinput example_data/inputs.json --resources example_data/resources.json --outdir results
```
